import express from 'express';
import { checkHealth } from './Controller/checkHealthController.js';
import { bio } from './Controller/BiodataController.js';
import { checkAuthKey } from './Middleware/checkKay.js';
import orderApi from './Api/orderApi.js';

const router = express.Router();

router.get('/check-health', checkHealth);
router.get('/biodata', checkAuthKey, bio);
router.use('/order', orderApi);

export default router;
