const orders = [];

class Order {
  constructor(params) {
    this.id = params.id;
    this.users = params.users;
    this.storage = params.storage;
    this.admin = params.admin;
    this.permintaan = params.permintaan;
  }

  static list() {
    return orders;
  }

  static create(params) {
    const order = new this(params);

    orders.push(order);

    return order;
  }

  static find(id) {
    const order = orders.find((i) => i.id === Number(id));
    if (!order) return null;

    return order;
  }
}

export default Order;
