import Order from '../Resources/Order.js';

export const order = (req, res) => {
  const orders = Order.order();

  return res.status(200).json(orders);
};

export const orders = (req, res) => {
  const order = Order.orders(req.params.id);

  return res.status(200).json(order);
};

export const archive = (req, res) => {
  const order = Order.archive(req.params.id);

  return res.status(200).json(order);
};
