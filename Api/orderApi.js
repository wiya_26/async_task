import express from 'express';
import { order, orders, archive } from '../Controller/OrderController.js';

const router = express.Router();

router.post('/order', order);
router.get('/order', order);
router.get('/orders/:id', orders);
router.put('/orders/:id', orders);
router.delete('/orders/:id', orders);
router.put('/archive/:id', archive);

export default router;
